package com.geekhub.task2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

class ZipArchive {
    private static Scanner scanner = new Scanner(System.in);

    void createZipFile(File directoryToZip) {
        String[] imageFileExtensions = {".jpg", ".gif", ".png", ".jpeg"};
        String[] audioFileExtensions = {".mp3", ".wav", ".wma"};
        String[] videoFileExtensions = {".flv", ".avi", ".mp4"};
        System.out.println("~~Searching for files~~");
        List<File> fileList = getAllFiles(directoryToZip);
        System.out.println("~~Creating archives~~");
        createZip(directoryToZip, fileList, "hw6\\src\\main\\resources/images.zip", imageFileExtensions);
        createZip(directoryToZip, fileList, "hw6\\src\\main\\resources/audios.zip", audioFileExtensions);
        createZip(directoryToZip, fileList, "hw6\\src\\main\\resources/videos.zip", videoFileExtensions);
    }

    File tryToEnterThePath() {
        File directoryToZip;
        do {
            directoryToZip = new File(enterPath());
        } while (!directoryToZip.exists());
        return directoryToZip;
    }

    private void createZip(File directoryToZip, List<File> fileList, String pathToZip, String[] fileExtensions) {
        try (FileOutputStream fos = new FileOutputStream(pathToZip); ZipOutputStream zos = new ZipOutputStream(fos)) {
            for (File file : fileList) {
                if (!file.isDirectory() && file.getName().contains(fileExtensions[0]) || file.getName().contains(fileExtensions[1]) || file.getName().contains(fileExtensions[2])) {
                    int n = pathToZip.split("/").length - 1;
                    String name = pathToZip.split("/")[n];
                    addToZip(directoryToZip, file, zos, name);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addToZip(File directoryToZip, File file, ZipOutputStream zos, String zipName) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1);
        System.out.println("Writing '" + zipFilePath + "' to " + zipName);
        ZipEntry zipEntry = new ZipEntry(zipFilePath);
        zos.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }
        zos.closeEntry();
        fis.close();
    }

    private List<File> getAllFiles(File dir) {
        File[] files = dir.listFiles();
        return new ArrayList<>(Arrays.asList(Objects.requireNonNull(files)));
    }

    private String enterPath() {
        System.out.println("Enter the path to your directory: ");
        return scanner.nextLine();
    }
}


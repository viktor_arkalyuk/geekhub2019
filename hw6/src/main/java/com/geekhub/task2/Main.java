package com.geekhub.task2;

import java.io.File;

public class Main {

    private static ZipArchive utils = new ZipArchive();

    public static void main(String[] args) {
        File directoryToZip = utils.tryToEnterThePath();
        utils.createZipFile(directoryToZip);
    }
}

package com.geekhub.task1;

import java.io.*;

public class FileEncryption {

    public static void main(String[] args) {
        File file = new File("hw6\\src\\main\\resources\\com\\geekhub\\task1\\File.txt");
        try (FileReader fileReader = new FileReader(file)) {
            try (BufferedReader reader = new BufferedReader(fileReader)) {
                while (reader.readLine();
                while (line != null) {
                    String[] split = line.split("[\\., ]");
                    StringBuilder result = new StringBuilder();
                    for (String s : split) {
                        if (s.length() >= 10) {
                            String sum = String.valueOf(s.length() - 2);
                            s = s.charAt(0) + sum + s.charAt(s.length() - 1);
                        } else {
                            s = "";
                        }
                        result.append(s).append(",");
                    }
                    try (FileWriter writer = new FileWriter("hw6\\src\\main\\resources\\com\\geekhub\\task1\\EncryptionFile.txt", false)) {
                        writer.write(result.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    line = reader.readLine();
                    fileReader.close();
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
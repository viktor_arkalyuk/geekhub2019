package com.geekhub.task2.storage;

import com.geekhub.task2.objects.Entity;
import com.geekhub.task2.objects.Ignore;
import com.geekhub.task2.objects.Name;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        StringBuilder sql =  new StringBuilder("SELECT * FROM ");
        if (clazz.isAnnotationPresent(Name.class)) {
            sql.append("\"").append(clazz.getAnnotation(Name.class).value()).append("\"");
        } else {
            sql.append("\"").append(clazz.getSimpleName().toLowerCase()).append("\"");
        }   sql.append("WHERE id = ").append(id);
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql.toString()));
            return result.isEmpty() ? null : result.get(0);
        }
    }


    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        if (clazz.isAnnotationPresent(Name.class)) {
            sql.append("\"").append(clazz.getAnnotation(Name.class).value()).append("\"");
        } else {
            sql.append("\"").append(clazz.getSimpleName().toLowerCase()).append("\"");
        }
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql.toString()));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) {
        StringBuilder sql = new StringBuilder("DELETE FROM ");
        if (entity.getClass().isAnnotationPresent(Name.class)) {
            sql.append("\"").append(entity.getClass().getAnnotation(Name.class).value()).append("\"");
        } else {
            sql.append("\"").append(entity.getClass().getSimpleName().toLowerCase()).append("\"");
        }   sql.append(" WHERE id = ").append(entity.getId());
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql.toString()) != 0;
        } catch (SQLException e) {
            return false;
        }
    }

    public <T extends Entity> void insert(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        StringBuilder sql = new StringBuilder("INSERT INTO ");
        if (entity.getClass().isAnnotationPresent(Name.class)) {
            sql.append("\"").append(entity.getClass().getAnnotation(Name.class).value()).append("\"");
        } else {
            sql.append("\"").append(entity.getClass().getSimpleName().toLowerCase()).append("\"");
        }

        StringJoiner fields = new StringJoiner(",", "(", ")");
        StringJoiner values = new StringJoiner(",", "(", ")");
        for (String field : data.keySet()) {
            fields.add(field);
            values.add("?");
        }
        sql.append(fields.toString());
        sql.append(" VALUES ");
        sql.append(values.toString());
        int count;
        try (PreparedStatement stmt = connection.prepareStatement(
                sql.toString(),
                Statement.RETURN_GENERATED_KEYS)
        ) {
            int i = 1;
            for (String field : data.keySet()) {
                stmt.setObject(i++, data.get(field));
            }
            count = stmt.executeUpdate();
            if (count > 0) {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                if (null != generatedKeys && generatedKeys.next()) {
                    entity.setId(generatedKeys.getInt(1));
                }
            }
        }
    }

    public <T extends Entity> void update(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        StringBuilder buildSql = new StringBuilder("UPDATE ");
        if (entity.getClass().isAnnotationPresent(Name.class)) {
            buildSql.append("\"").append(entity.getClass().getAnnotation(Name.class).value()).append("\"");
        } else {
            buildSql.append("\"").append(entity.getClass().getSimpleName().toLowerCase()).append("\"");
        }
        buildSql.append(" SET ");
        StringJoiner values = new StringJoiner(",");
        for (String field : data.keySet()) {
            values.add(field + "=?");
        }
        buildSql.append(values.toString());
        buildSql.append(" WHERE id=").append(entity.getId());

        try (PreparedStatement stmt = connection.prepareStatement(buildSql.toString())) {
            int i = 1;
            for (String field : data.keySet()) {
                stmt.setObject(i++, data.get(field));
            }
            stmt.executeUpdate();
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        if (entity.isNew()) {
            insert(entity);
        } else {
            update(entity);
        }
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();

        for (Field field : entity.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Ignore.class)) continue;
            if (field.isAnnotationPresent(Name.class)) {
                field.setAccessible(true);
                data.put(field.getAnnotation(Name.class).value(), field.get(entity));
            }
            field.setAccessible(true);
            data.put(field.getName(), field.get(entity));
        }
        return data;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> result = new ArrayList<>();
        Field[] fields = clazz.getSuperclass().getDeclaredFields();
        ArrayList<Field> allFields = new ArrayList<>(Arrays.asList(fields));
        allFields.addAll(Arrays.asList(clazz.getDeclaredFields()));

        while (resultSet.next()) {
            T object = clazz.newInstance();

            for (Field field : allFields) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Ignore.class)) {
                    continue;
                }
                Object fieldValue = resultSet.getObject(field.getName());
                field.set(object, fieldValue);
            }
            result.add(object);
        }
        return result;
    }
}

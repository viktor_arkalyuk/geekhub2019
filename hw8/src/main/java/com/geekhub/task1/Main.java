package com.geekhub.task1;

import java.sql.*;

public class Main {
    public static void main(String[] args) throws SQLException {
        Connection c = null;
        String user = "postgres";
        String password = "vitya22102000";
        try {
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            user, password);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        Statement stmt = c.createStatement();

//        putValuesInCustomers(stmt, "VALUES ('Paul', 'Rodgers', '380123123123' )");
//        putValuesInCustomers(stmt, "VALUES ('Tony', 'Stark', '380685856252')");
//        putValuesInCustomers(stmt, "VALUES ('Natasha', 'Romanov', '380685856224')");
//        putValuesInCustomers(stmt, "VALUES ('Johny', 'Jason', '380685856211')");
//
//        putValuesInProducts(stmt, "VALUES ('Phone','Iphone', 10000)");
//        putValuesInProducts(stmt, "VALUES('Car', 'BMW', 50000)");
//        putValuesInProducts(stmt, "VALUES('Products', 'Tomato', 20 )");
//        putValuesInProducts(stmt, "VALUES('Medicament', 'Korvalol', 100)");
//
//        putValuesInOrders(stmt, "VALUES( 1, 1, 5, 'Kiev')");
//        putValuesInOrders(stmt, "VALUES( 2, 2, 1, 'Cherkasy')");
//        putValuesInOrders(stmt, "VALUES( 3, 3, 20, 'Lviv')");

        ResultSet resultSet = stmt.executeQuery(
                "SELECT * FROM customers " +
                        "JOIN orders o ON customers.id = o.customer_id " +
                        "JOIN products p ON customers.id = p.id"
        );
        while (resultSet.next()) {
            int customer_id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String cellPhone = resultSet.getString("cell_phone");
            String name = resultSet.getString("name");
            int currentPrice = resultSet.getInt("current_price");

            System.out.println(
                    "Customer id = " + customer_id +
                            ", First name = " + firstName +
                            ", Last name = " + lastName +
                            ", Cell phone = " + cellPhone +
                            ", Name products = " + name +
                            ", Order price = " + currentPrice
            );
        }
        ResultSet resultSet1 = stmt.executeQuery("SELECT products_id FROM orders WHERE quantity= (SELECT MAX(quantity) FROM orders)");
        while (resultSet1.next()) {
            int productsId = resultSet1.getInt("products_id");
            System.out.println("Most popular products have id = " + productsId);
        }
        c.close();
    }

    private static void putValuesInProducts(Statement stmt, String s) throws SQLException {
        String sql = "INSERT INTO PRODUCTS (name, description, current_price)" + s;
        stmt.executeUpdate(sql);
    }

    private static void putValuesInCustomers(Statement stmt, String s) throws SQLException {
        String sql = "INSERT INTO CUSTOMERS (first_name,last_name,cell_phone)" + s;
        stmt.executeUpdate(sql);
    }

    private static void putValuesInOrders(Statement stmt, String s) throws SQLException {
        String sql = "INSERT INTO orders (customer_id, products_id, quantity, delivery_place) " + s;
        stmt.executeUpdate(sql);
    }
}
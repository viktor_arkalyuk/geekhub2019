-- script 1
SELECT * FROM (SELECT c.name, c.id, c.phonecode, c.sortname, count(states.country_id)
FROM countries as c
JOIN states ON states.country_id = c.id
GROUP BY c.id) as cs
ORDER BY cs.count DESC
LIMIT 1;

-- script 2
SELECT c.id, c.sortname, c.name, c.phonecode, count(ci.id)
FROM countries c JOIN states s ON c.id = s.country_id
                 JOIN cities ci ON s.id = ci.state_id
GROUP BY c.id, c.sortname, c.name, c.phonecode
ORDER BY count(ci.id) DESC
LIMIT 1;

-- script 3
SELECT c.id, c.sortname, c.name, c.phonecode, count(s.id)
FROM countries c JOIN states s ON c.id = s.country_id
GROUP BY c.id, c.sortname, c.name, c.phonecode
ORDER BY count(s.id), c.name, c.id DESC;

-- script 4
SELECT c.id, c.sortname, c.name, c.phonecode, count(ci.id)
FROM countries c JOIN states s ON c.id = s.country_id
                 JOIN cities ci ON s.id = ci.state_id
GROUP BY c.id, c.sortname, c.name, c.phonecode
ORDER BY count(ci.id), c.name, c.id DESC;

--script 5
SELECT c.name, c.phonecode,
       (SELECT count(*) FROM states s
        WHERE s.country_id = c.id) count_states,
       (SELECT count(*) FROM cities ci
        WHERE ci.state_id = s.id) count_cities
FROM countries c JOIN states s ON c.id = s.country_id;

--script 6
SELECT c.id, c.sortname, c.name, c.phonecode, count(ci.id)
FROM countries c JOIN states s ON c.id = s.country_id
                 JOIN cities ci ON s.id = ci.state_id
GROUP BY c.id, c.sortname, c.name, c.phonecode
ORDER BY count(ci.id) DESC
LIMIT 10;

--script 7
WITH col AS (SELECT * FROM
                           (SELECT c.name, c.id, c.phonecode, c.sortname, count(states.country_id)
                            FROM countries as c
                            JOIN states ON states.country_id = c.id
                            GROUP BY c.id) as cs)
SELECT * FROM
              (SELECT * FROM col
              ORDER BY col.count DESC
              LIMIT 10) as c2
UNION SELECT * FROM (SELECT * FROM col ORDER BY col.count LIMIT 10) as c3
ORDER BY name;


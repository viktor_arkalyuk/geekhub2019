CREATE DATABASE database;

CREATE TABLE customers (
    Id SERIAL PRIMARY KEY,
    First_name CHARACTER VARYING(30),
    Last_name CHARACTER VARYING(30),
    Cell_phone CHARACTER VARYING(30)
);

CREATE TABLE products (
    Id SERIAL PRIMARY KEY,
    Name CHARACTER VARYING(30),
    Description CHARACTER VARYING(40),
    Current_price INTEGER NOT NULl
);

CREATE TABLE orders (
    Id SERIAL PRIMARY KEY,
    Customer_id INTEGER REFERENCES customers(id),
    Products_id INTEGER REFERENCES products(id),
    Quantity INTEGER,
    Delivery_place CHARACTER VARYING(100)
)
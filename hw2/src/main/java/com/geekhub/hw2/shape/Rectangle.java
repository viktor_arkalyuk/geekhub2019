package com.geekhub.hw2.shape;

public class Rectangle implements Shape {
    private final double a;
    private final double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double calculateArea() {
        return a * b;
    }

    public double calculatePerimeter() {
        return 2 * (a + b);
    }

    public double triangleArea() {
        return a * b / 2;
    }
}

package com.geekhub.hw2.shape;

interface Shape {
    double calculateArea();
    double calculatePerimeter();
}



package com.geekhub.hw2.shape;

public class Square implements Shape {
    private final double a;

    public Square(double a) {
        this.a = a;
    }

    public double calculateArea() {
        return Math.pow(a, 2);
    }

    public double calculatePerimeter() {
        return 4 * a;
    }

    public double triangleArea() {
        return Math.pow(a, 2) / 2;
    }
}


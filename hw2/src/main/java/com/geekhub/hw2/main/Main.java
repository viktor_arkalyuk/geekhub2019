package com.geekhub.hw2.main;

import com.geekhub.hw2.shape.Circle;
import com.geekhub.hw2.shape.Rectangle;
import com.geekhub.hw2.shape.Square;
import com.geekhub.hw2.shape.Triangle;

import java.util.Scanner;

public class Main {
    public enum Figure {
        CIRCLE,
        TRIANGLE,
        SQUARE,
        RECTANGLE
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter name of shape:");
        String s = sc.nextLine();
        boolean shapeExists = false;
        for (Figure shape : Figure.values()) {
            if (s.equalsIgnoreCase(shape.name())) {
                shapeExists = true;
                break;
            }
        }
        if (!shapeExists) {
            System.out.println("It's not shape!");
        }
        switch (s) {
            case "Circle":
                System.out.println("Enter the radius circle:");
                int r = sc.nextInt();
                Circle circle = new Circle(r);
                System.out.println("Resulting area: " + circle.calculateArea() + "  " + "Resulting perimeter: " + circle.calculatePerimeter());
                break;
            case "Triangle":
                System.out.println("Enter the sides triangle:");
                int a = sc.nextInt();
                int b = sc.nextInt();
                int c = sc.nextInt();
                Triangle triangle = new Triangle(a, b, c);
                System.out.println("Resulting area: " + triangle.calculateArea() + "  " + "Resulting perimeter: " + triangle.calculatePerimeter());
                break;
            case "Square":
                System.out.println("Enter the side square:");
                int side = sc.nextInt();
                Square square = new Square(side);
                System.out.println("Resulting area: " + square.calculateArea() + "  " + "Resulting perimeter: " + square.calculatePerimeter());
                System.out.println("Square with 2 triangles of area:" + square.triangleArea());
                break;
            case "Rectangle":
                System.out.println("Enter width and length rectangle:");
                int w = sc.nextInt();
                int l = sc.nextInt();
                Rectangle rectangle = new Rectangle(w, l);
                System.out.println("Resulting area:" + rectangle.calculateArea() + "  " + "Resulting perimeter:" + rectangle.calculatePerimeter());
                System.out.println("Square with 2 triangles of area:" + rectangle.triangleArea());
                break;
        }

    }
}


package com.geekhub.hw4.linked;

import com.geekhub.hw4.list.List;

import java.util.Iterator;

public class LinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;
    private int size;

    LinkedList() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);
        if (tail == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean add(int index, E element) {
        Node<E> node = new Node<>(element, null);
        if (head == null) {
            this.head = node;
            this.tail = node;
            size++;
            return true;
        }
        Node<E> head = this.head;

        if (index == 0) {
            this.head = node;
            this.head.next = head;
            size++;
            return true;
        }

        for (int i = 1; i < index && head.getNext() != null; i++) {
            head = head.getNext();
        }
        node.setNext(head.getNext());
        head.setNext(node);
        if (node.getNext() == null) {
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        for (E element : elements) {
            Node<E> node = new Node<>(element, null);
            if (tail == null) {
                head = node;
                tail = node;
            } else {
                tail.next = node;
                tail = node;
            }
        }
        size += elements.size();
        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {

        if (head == null) {
            for (E element : elements) {
                Node<E> node = new Node<>(element, null);
                this.head = node;
                this.tail = node;
            }
            size += elements.size();
            return true;
        }

        Node<E> currentHead = this.head;

        if (index == 0) {
            Node<E> head = null;
            for (E element : elements) {
                Node<E> current = new Node<>(element, null);
                if (head == null) {
                    current.setNext(currentHead);
                    this.head = current;
                    head = current;
                } else {
                    current.setNext(currentHead);
                    head.setNext(current);
                    head = head.getNext();
                }
            }
            size += elements.size();
            return true;
        }

        if (index == size) {
            for (E element : elements) {
                Node<E> current = new Node<>(element, null);
                currentHead.setNext(current);
                currentHead = currentHead.getNext();
                if (currentHead.getNext() == null) {
                    tail = current;
                }
            }
            size += elements.size();
            return true;
        }

        for (int i = 1; i < index && currentHead.getNext() != null; i++) {
            currentHead = currentHead.getNext();
        }

        Node<E> nextHead = currentHead.getNext();
        for (E element : elements) {
            Node<E> current = new Node<>(element, null);
            currentHead.setNext(current);
            currentHead = currentHead.getNext();
        }
        currentHead.setNext(nextHead);
        size += elements.size();
        return true;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public E remove(int index) {
        if (head == null) {
            System.out.println("List is empty");
            return null;
        }
        if (index <= 1) {
            head = head.getNext();
        } else {
            Node<E> node = head;
            for (int i = 0; i < index - 1; i++) {
                node = node.getNext();
            }
            node.setNext(node.getNext().getNext());
            size--;
            return head.element;
        }
        return null;
    }

    @Override
    public E remove(E element) {
        if (head == null) throw new RuntimeException("Cannot delete: list is already empty!");

        Node<E> previous = null;
        Node<E> current = head;

        while (current != null) {
            if (current.element.equals(element)) {
                if (previous != null) {
                    previous.next = current.next;

                    if (current.next == null) {
                        tail = previous;
                    }
                } else {
                    head = current.next;

                    if (head == null) {
                        tail = null;
                    }
                }
                size--;
                return current.element;
            }
            previous = current;
            current = current.next;
        }
        return null;
    }

    @Override
    public E get(int index) {
        Node<E> curr = this.head;
        for (int i = 0; i < this.size(); i++) {
            if (i == index) {
                return curr.element;
            }
            curr = curr.next;
            if (index > size) {
                return null;
            }
            if (index < 0) {
                return null;
            }
        }
        return null;

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int indexOf(E element) {
        if (size == 0) {
            return -1;
        }
        for (int i = 0; i < size; i++) {
            if (element.equals(get(i))) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(E element) {
        for (int i = 0; i < size; i++) {
            if (get(i) == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator(head);
    }
}

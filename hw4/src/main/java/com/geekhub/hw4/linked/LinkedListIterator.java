package com.geekhub.hw4.linked;

import java.util.Iterator;

public class LinkedListIterator<E> implements Iterator {
    private Node<E> head;

    LinkedListIterator(Node<E> head) {
        this.head = head;
    }

    @Override
    public boolean hasNext() {
        return head != null;
    }

    @Override
    public E next() {
        Node<E> node = this.head;
        this.head = node.next;
        return node.element;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        for (LinkedListIterator<E> it = this; it.hasNext(); ) {
            E element = it.next();
            s.append(element).append(", ");
        }
        return s.toString();
    }
}

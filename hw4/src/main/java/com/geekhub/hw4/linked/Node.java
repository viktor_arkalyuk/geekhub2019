package com.geekhub.hw4.linked;

public class Node<E> {
    E element;
    Node<E> next;

    Node(E element, Node<E> next) {
        this.element = element;
        this.next = next;
    }

    E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    Node<E> getNext() {
        return next;
    }

    void setNext(Node<E> next) {
        this.next = next;
    }
}

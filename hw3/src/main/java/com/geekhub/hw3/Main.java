package com.geekhub.hw3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("How many customer you want to input : ");
        int n = sc.nextInt();
        Bank[] C = new Bank[n];
        for (int i = 0; i < C.length; i++) {
            C[i] = new Bank();
            C[i].openAccount();
        }

        int ch;
        do {
            System.out.println("Main Menu\n  " +
                    "1.Search by account and output your balance\n  " +
                    "2.Open deposit\n  " +
                    "3. Check all sum active in the bank\n  " +
                    "4.Exit");
            System.out.println("Your choice :");
            ch = sc.nextInt();
            switch (ch) {
                case 1:
                    System.out.print("Enter account number you want to search: ");
                    String acn = sc.next();
                    boolean found = false;
                    for (Bank bank : C) {
                        found = bank.search(acn);
                        if (found) {
                            break;
                        }
                    }
                    if (!found) {
                        System.out.println("Search failed. Account not exist.");
                    }
                    break;

                case 2:
                    System.out.print("Enter account number : ");
                    acn = sc.next();
                    found = false;
                    for (Bank bank : C) {
                        found = bank.search(acn);
                        if (found) {
                            bank.deposit();
                            break;
                        }
                    }
                    if (!found) {
                        System.out.println("Search failed. Account not exist.");
                    }
                    break;
                case 3:
                    double sum = 0;
                    for (Bank bank : C) {
                        sum += bank.getBalance();
                    }
                    System.out.println("All sum active: " + sum);
                    break;
                case 4:
                    System.out.println("Good Bye.");
                    break;
            }
        }
        while (ch != 4);
    }
}

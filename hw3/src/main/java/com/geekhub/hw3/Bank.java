package com.geekhub.hw3;

import java.time.LocalDate;
import java.util.Scanner;

class Bank {
    private String accNum;
    private String name;
    private double balance;

    private Scanner sc = new Scanner(System.in);

    void openAccount() {
        System.out.println("Choose legal or physical person");
        String kind = sc.nextLine();
        switch (kind) {
            case "Legal":
                createLegalAccount();
                break;
            case "Physical":
                createPhysicalAccount();
                break;
        }
    }

    private void createPhysicalAccount() {
        System.out.print("Enter account number: ");
        accNum = sc.next();
        System.out.print("Enter your name: ");
        name = sc.next();
        System.out.print("Enter your age: ");
        int age = sc.nextInt();
        PhysicalPerson physicalPerson = new PhysicalPerson(accNum, name, age);
        physicalPerson.info();
    }

    private void createLegalAccount() {
        System.out.print("Enter account number: ");
        accNum = sc.next();
        System.out.print("Enter your official name: ");
        name = sc.next();
        System.out.print("Enter your location:");
        String location = sc.next();
        LegalPerson legalPerson = new LegalPerson(accNum, name, location, true);
        legalPerson.info();
    }

    private void showActive() {
        System.out.println("Balance: " + balance);
    }

    void deposit() {
        LocalDate localDate = LocalDate.of(2019, 10, 23);
        System.out.println("Star date deposit: " + localDate);
        System.out.println("Choose your currency deposit:");
        String dep = sc.nextLine();
        switch (dep) {
            case "Dollar":
                System.out.println("Enter your start balance:");
                balance = sc.nextInt();
                openDeposit(localDate);
                break;
            case "Precious metal":
                System.out.println("Enter your start balance in kg:");
                balance = sc.nextInt();
                openDeposit(localDate);
        }
    }

    private void openDeposit(LocalDate localDate) {
        System.out.println("Enter how long your deposit:");
        int n = sc.nextInt();
        localDate = localDate.plusYears(n);
        System.out.println("When end your deposit: " + localDate);
        balance = balance * (n * 1.2);
    }

    boolean search(String acn) {
        if (accNum.equals(acn)) {
            showActive();
            return (true);
        }
        return (false);
    }

    double getBalance() {
        return balance;
    }
}

package com.geekhub.hw3;

class LegalPerson {
    private String accNumber;
    private String officialName;
    private String location;
    private boolean availabilityOfProperty;

    LegalPerson(String accNumber, String officialName, String location, boolean availabilityOfProperty) {
        this.accNumber = accNumber;
        this.officialName = officialName;
        this.location = location;
        this.availabilityOfProperty = availabilityOfProperty;
    }

    void info() {
        System.out.println("Account number: " + accNumber);
        System.out.println("Official name customer: " + officialName);
        System.out.println("Location customer: " + location);
        System.out.println("Availability of property: " + availabilityOfProperty);
    }
}



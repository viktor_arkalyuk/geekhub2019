package com.geekhub.hw3;

class PhysicalPerson {
    private String accNumber;
    private String name;
    private int age;

    PhysicalPerson(String accNumber, String name, int age) {
        this.accNumber = accNumber;
        this.name = name;
        this.age = age;
    }

    void info() {
        System.out.println("Account number: " + accNumber);
        System.out.println("Name customer: " + name);
        System.out.println("Age customer: " + age);
    }
}

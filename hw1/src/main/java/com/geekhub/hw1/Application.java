package com.geekhub.hw1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application {

    private static void getSum(String s) {
        int sum = 0;
        for (int i = 0, n = s.length(); i < n; i++) {
            sum += Character.getNumericValue(s.charAt(i));
        }
        System.out.println("1st round of calculation sum is:" + sum);
        do {
            int currentNumber = sum;
            sum = 0;
            while (currentNumber >= 10) {
                sum += (currentNumber % 10);
                currentNumber /= 10;
            }
            sum += currentNumber;
            System.out.println("2st round of calculation sum is:" + sum);
        }
        while (sum >= 10);

        switch (sum) {
            case 1:
                System.out.println("Final result is: One");
                break;
            case 2:
                System.out.println("Final result is: Two");
                break;
            case 3:
                System.out.println("Final result is: Three");
                break;
            case 4:
                System.out.println("Final result is: Four");
                break;
            default:
                System.out.println("Final result is:" + sum);
                break;

        }
    }

    private static boolean ToCheckNumber(String s) {
        Pattern p = Pattern.compile("^\\+?3?8?(0(67|68|96|97|98|73|63|93|66|95|99)\\d{7})$");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    public static void main(String[] args) {
        System.out.println("Enter your phone number:");
        Scanner sc = new Scanner(System.in);
        String s;

        do {
            s = sc.nextLine();
            System.out.println("Phone number is incorrect. Please enter you phone number:");
        }
        while (!ToCheckNumber(s));
        System.out.println("Phone number is correct.");
        getSum(s);
    }
}






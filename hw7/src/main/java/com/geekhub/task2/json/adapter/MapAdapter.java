package com.geekhub.task2.json.adapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.geekhub.task2.json.JsonSerializer.serialize;

/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */
public class MapAdapter implements JsonDataAdapter<Map> {

    @Override
    public Object toJson(Map map) throws JSONException {

        JSONObject jsonObject = new JSONObject();

        for (Object key : map.keySet()) {
            jsonObject.put(key.toString(), serialize(map.get(key)));
        }

        return jsonObject;
    }
}

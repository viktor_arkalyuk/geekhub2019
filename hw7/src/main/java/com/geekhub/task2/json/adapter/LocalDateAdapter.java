package com.geekhub.task2.json.adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Converts object of type java.time.LocalDate to String by using ISO 8601 format
 */
public class LocalDateAdapter implements JsonDataAdapter<LocalDate> {

    @Override
    public Object toJson(LocalDate date) {

        DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return date.format(dt);
    }
}

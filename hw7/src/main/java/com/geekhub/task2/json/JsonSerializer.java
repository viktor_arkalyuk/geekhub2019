package com.geekhub.task2.json;

import com.geekhub.task2.json.adapter.UseDataAdapter;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * JsonSerializer converts Java objects to JSON representation.
 */
public class JsonSerializer {

    /**
     * simpleTypes contains java classes for which we should not make any deeper serialization and we should return object as is
     * and use toString() method to get it serialized representation
     */
    private static List<Class> simpleTypes = Arrays.asList(
            String.class,
            Integer.class,
            Short.class,
            Long.class,
            Byte.class,
            Double.class,
            Float.class,
            Character.class,
            Boolean.class,
            int.class,
            short.class,
            long.class,
            byte.class,
            double.class,
            float.class,
            char.class,
            boolean.class
    );

    /**
     * Main method to convert Java object to JSON. If type of the object is part of the simpleTypes object itself will be returned.
     * If object is null String value "null" will be returned.
     *
     * @param o object to serialize.
     * @return JSON representation of the object.
     */
    public static Object serialize(Object o) {
        if (null == o) {
            return "null";
        }
        if (simpleTypes.contains(o.getClass())) {
            return o;
        } else {
            try {
                return toJsonObject(o);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * Converts Java object to JSON. Uses reflection to access object fields.
     * Uses JsonDataAdapter to serialize complex values. Ignores @Ignore annotated fields.
     *
     * @param o object to serialize to JSON
     * @return JSON object.
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static JSONObject toJsonObject(Object o) throws InstantiationException, IllegalAccessException {

        JSONObject jsonObject = new JSONObject();
        Field[] declaredFields = o.getClass().getDeclaredFields();

        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            if (declaredField.isAnnotationPresent(Ignore.class)) {
                continue;
            }
            if (declaredField.isAnnotationPresent(UseDataAdapter.class)) {
                UseDataAdapter annotation = declaredField.getAnnotation(UseDataAdapter.class);

                Object dataAdapter = annotation
                        .value()
                        .newInstance()
                        .toJson(declaredField.get(o));
                jsonObject.put(declaredField.getName(), dataAdapter);
            } else if (simpleTypes.contains(declaredField.getType())) {
                jsonObject.put(declaredField.getName(), declaredField.get(o));
            }
        }
        return jsonObject;
    }
}

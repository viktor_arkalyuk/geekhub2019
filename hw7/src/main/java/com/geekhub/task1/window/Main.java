package com.geekhub.task1.window;

import com.geekhub.task1.beanUtils.BeanComparator;
import com.geekhub.task1.beanUtils.CloneCreator;
import com.geekhub.task1.type.Car;
import com.geekhub.task1.type.Cat;
import com.geekhub.task1.type.Human;
import com.geekhub.task1.beanUtils.BeanRepresenter;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException {

        Cat testCat = new Cat("Black", 4, 4, 120);
        Car testCar = new Car("Blue", 320, "Sport", "Mustang");
        Human testHuman = new Human(180, "male", 25, 60);

        BeanRepresenter.represent(testCat);

        CloneCreator.clone(testHuman);

        Car testCar2 = new Car("Black", 360, "Chevrolet", "Camaro");
        BeanComparator.compare(testCar2, testCar);
    }
}

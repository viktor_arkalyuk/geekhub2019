package com.geekhub.task1.products;

public class Products {
    private final String name;
    private final int price;
    private final int quantityOnHand;

    Products(String name, int costs, int quantityOnHand) {
        this.name = name;
        this.price = costs;
        this.quantityOnHand = quantityOnHand;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    int getQuantityOnHand() {
        return quantityOnHand;
    }
}

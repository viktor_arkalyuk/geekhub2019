package com.geekhub.task1.products;

import java.util.List;
import java.util.Scanner;

public class ProductsOnFabric {
    private Scanner scanner = new Scanner(System.in);

    public Products getNewProduct() {
        System.out.println("What kind of product you want to create?" +
                "\n 1. Phone" +
                "\n 2. Car" +
                "\n 3. Clothes" +
                "\n 4. Vegetable");
        int choice = scanner.nextInt();
        int costs, quantityOnHand;
        System.out.print("Enter product name: ");
        scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.print("Enter product price:");
        costs = scanner.nextInt();
        System.out.print("Enter product quantity on hand: ");
        quantityOnHand = scanner.nextInt();

        Products product = null;
        switch (choice) {
            case 1: {
                System.out.print("Enter volts usage: ");
                int voltsUsage = scanner.nextInt();
                product = new Phone(name, costs, quantityOnHand, voltsUsage);
                break;
            }
            case 2: {
                scanner = new Scanner(System.in);
                System.out.println("Enter seats in car: ");
                int seatsInCar = scanner.nextInt();
                product = new Car(name, costs, quantityOnHand, seatsInCar);
                break;
            }
            case 3: {
                System.out.println("Enter size: ");
                int size = scanner.nextInt();
                product = new Clothes(name, costs, quantityOnHand, size);
                break;
            }
            case 4: {
                scanner = new Scanner(System.in);
                System.out.println("Enter weight : ");
                int weight = scanner.nextInt();
                product = new Vegetable(name, costs, quantityOnHand, weight);
            }
        }
        return product;
    }

    public void getTestProducts(List<Products> list) {
        list.add(new Vegetable("Tomato", 15, 27, 20));
        list.add(new Clothes("T-shirt", 76, 1, 45));
        list.add(new Car("BMW", 200000, 20, 5));
        list.add(new Clothes("Leather shoes", 100, 2, 42));
        list.add(new Phone("Iphone", 270, 1, 20));
    }
}

package com.geekhub.task1.products;

public class Phone extends Products {

    private int voltsUsage;

    Phone(String name, int costs, int quantityOnHand, int voltsUsage) {
        super(name, costs, quantityOnHand);
        this.voltsUsage = voltsUsage;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + super.getName() + '\'' +
                ", costs=" + super.getPrice() +
                ", quantity on hand=" + super.getQuantityOnHand() +
                ", volts usage=" + voltsUsage +
                '}';
    }
}

package com.geekhub.task1.products;

class Vegetable extends Products {

    private int weight;

    Vegetable(String name, int price, int number, int weight) {
        super(name, price, number);
        this.weight = weight;
    }

    public String toString() {
        return "Vegetable{" +
                "name='" + super.getName() + '\'' +
                ", costs=" + super.getPrice() +
                ", quantity on hand=" + super.getQuantityOnHand() +
                ", weight=" + weight +
                '}';
    }
}

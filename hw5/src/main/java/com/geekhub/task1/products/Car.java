package com.geekhub.task1.products;

class Car extends Products {

    private int seatsInCar;

    Car(String name, int price, int number, int seatsInCar) {
        super(name, price, number);
        this.seatsInCar = seatsInCar;
    }
    public String toString() {
        return "Car{" +
                "name='" + super.getName() + '\'' +
                ", costs=" + super.getPrice() +
                ", quantity on hand=" + super.getQuantityOnHand() +
                ", seats in car=" + seatsInCar +
                '}';
    }
}

package com.geekhub.task1.products;

public class Clothes extends Products {

    private int size;

    Clothes(String name, int price, int number, int size) {
        super(name, price, number);
        this.size = size;
    }

    @Override
    public String toString() {
        return "Clothes{" +
                "name='" + super.getName() + '\'' +
                ", costs=" + super.getPrice() +
                ", quantity on hand=" + super.getQuantityOnHand() +
                ", size=" + size +
                '}';
    }
}

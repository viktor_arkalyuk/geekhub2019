package com.geekhub.task1.inventory;

import com.geekhub.task1.products.Products;
import com.geekhub.task1.products.ProductsOnFabric;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Inventory {
    private List<Products> list = new ArrayList<>();
    private ProductsOnFabric productsFabric = new ProductsOnFabric();

    void addNewProduct() {
        list.add(productsFabric.getNewProduct());
    }

    @Override
    public String toString() {
        String result = "";
        if (list.isEmpty()) {
            return "List is empty";
        }
        for (int i = 0; i < list.size(); i++) {
            result += i + 1 + ") " + list.get(i).toString() + "\n";
        }
        return result;
    }

    void setTestListOfProducts() {
        productsFabric.getTestProducts(list);
    }

    void sortedProductsByPrice() {
        List<Products> sortedList = list.stream()
                .sorted(Comparator.comparing(Products::getPrice))
                .collect(Collectors.toList());
        System.out.println("Sorted list by cost: " + sortedList);
    }

    void sortProductsByName() {
        List<Products> sortedList = list.stream()
                .sorted(Comparator.comparing(Products::getName))
                .collect(Collectors.toList());
        System.out.println("Sorted list by name: " + sortedList);
    }

    int getTheInventoryValue() {
        int inventoryValue = 0;
        for (Products products : list) {
            inventoryValue += products.getPrice();
        }
        return inventoryValue;
    }
}

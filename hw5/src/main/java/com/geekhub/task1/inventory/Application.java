package com.geekhub.task1.inventory;

import java.util.Scanner;

public class Application {
    private static Scanner scanner = new Scanner(System.in);
    private static Inventory inventory = new Inventory();

    public static void main(String[] args) {
        run();
    }

    private static void run() {
        while (true) {
            System.out.println("1. Print inventory" +
                    "\n2. Add test list of products" +
                    "\n3. Add new product" +
                    "\n4. Get the inventory value" +
                    "\n5. Sort the inventory by cost " +
                    "\n6. Sort the inventory by name" +
                    "\n7. Exit");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1: {
                    System.out.println(inventory);
                    break;
                }
                case 2: {
                    inventory.setTestListOfProducts();
                    break;
                }
                case 3: {
                    inventory.addNewProduct();
                    break;
                }
                case 4: {
                    System.out.println("The inventory value = " + inventory.getTheInventoryValue());
                    break;
                }
                case 5: {
                    inventory.sortedProductsByPrice();
                    break;
                }
                case 6: {
                    inventory.sortProductsByName();
                    break;
                }
                case 7: {
                    return;
                }
            }
        }
    }
}

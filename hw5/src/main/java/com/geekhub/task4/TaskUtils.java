package com.geekhub.task4;

import java.util.*;
import java.util.stream.Collectors;

class TaskUtils {
    List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(x1 -> x1.getType() == Task.TaskType.IMPORTANT)
                .sorted((x1, x2) -> x2.getStartsOn().compareTo(x1.getStartsOn()))
                .limit(5)
                .collect(Collectors.toList());
    }

    List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .map(Task::getCategories)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

    }

    Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(task -> task.getCategories().toString(), Collectors.toList()));
    }

    Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.partitioningBy(Task::isDone));
    }

    boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
                .map(Task::getCategories)
                .flatMap(Collection::stream)
                .anyMatch((x) -> x.equals(category));
    }

    String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .skip(startNo - 1)
                .limit(endNo)
                .map(Task::getTitle)
                .reduce((x1, x2) -> x1 + ", " + x2)
                .orElse("");

    }

    Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(task -> task.getCategories().toString(), Collectors.counting()));
    }

    IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return getUniqueCategories(tasks).stream()
                .collect(Collectors.summarizingInt(String::length));
    }

    Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .max(Comparator.comparingInt(task -> task.getCategories().size())).orElse(null);
    }
}

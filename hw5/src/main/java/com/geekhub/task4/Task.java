package com.geekhub.task4;

import java.time.LocalDate;
import java.util.Set;

public class Task {
    private int id;
    private TaskType type;
    private String title;
    private boolean done;
    private Set<String> categories;
    private LocalDate startsOn;

    public enum TaskType {
        IMPORTANT, UNIMPORTANT
    }

    Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startsOn = startsOn;

    }

    public int getId() {
        return id;
    }

    TaskType getType() {
        return type;
    }

    String getTitle() {
        return title;
    }

    boolean isDone() {
        return done;
    }

    Set<String> getCategories() {
        return categories;
    }

    public int getSizeCategories() {
        return categories.size();
    }

    LocalDate getStartsOn() {
        return startsOn;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", done=" + done +
                ", categories=" + categories +
                ", startsOn=" + startsOn +
                '}';
    }
}

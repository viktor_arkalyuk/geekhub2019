package com.geekhub.task4;

import java.time.LocalDate;
import java.util.*;

public class TaskManager {

    public static void main(String[] args) {
        TaskUtils stream = new TaskUtils();
        List<Task> tasks = new LinkedList<>();
        Set<String> sport = new HashSet<>();
        sport.add("Sport");
        Set<String> Medicine = new HashSet<>();
        Medicine.add("Medicine");
        Set<String> studyAndMedicine = new HashSet<>();
        studyAndMedicine.add("Study");
        studyAndMedicine.add("Medicine");
        Set<String> hobby = new HashSet<>();
        hobby.add("Hobby");
        tasks.add(new Task(1, Task.TaskType.IMPORTANT, "Go to fridge", false, sport, LocalDate.of(2019, 11, 5)));
        tasks.add(new Task(2, Task.TaskType.UNIMPORTANT, "Buy medicament", false, Medicine, LocalDate.of(2019, 2, 2)));
        tasks.add(new Task(3, Task.TaskType.UNIMPORTANT, "Learn Java", true, studyAndMedicine, LocalDate.of(2019, 11, 14)));
        tasks.add(new Task(4, Task.TaskType.IMPORTANT, "Play football", false, hobby, LocalDate.of(2019, 9, 1)));
        tasks.add(new Task(5, Task.TaskType.IMPORTANT, "Play basketball", true, hobby, LocalDate.of(2018, 11, 12)));
        tasks.add(new Task(6, Task.TaskType.IMPORTANT, "Play volleyball", false, hobby, LocalDate.of(2019, 11, 13)));
        tasks.add(new Task(7, Task.TaskType.IMPORTANT, "Play football", true, hobby, LocalDate.of(2019, 10, 15)));

        List<Task> nearestImportantTasks = stream.find5NearestImportantTasks(tasks);
        System.out.println("5 nearest important tasks: " + nearestImportantTasks);

        String titlesOfTasks = stream.getTitlesOfTasks(tasks, 1, 5);
        System.out.println("Title of tasks: " + titlesOfTasks);

        Map<String, List<Task>> categoriesWithTasks = stream.getCategoriesWithTasks(tasks);
        System.out.println("Categories with tasks: " + categoriesWithTasks);

        Map<String, Long> countsByCategories = stream.getCountsByCategories(tasks);
        System.out.println("Count by categories: " + countsByCategories);

        List<String> uniqueCategories = stream.getUniqueCategories(tasks);
        System.out.println("Unique categories: " + uniqueCategories);

        Map<Boolean, List<Task>> booleanListMap = stream.splitTasksIntoDoneAndInProgress(tasks);
        System.out.println("Tasks done and in progress: " + booleanListMap);

        boolean sports = stream.existsTaskOfCategory(tasks, "Sport");
        System.out.println("Exists tasks of category: " + sports);

        IntSummaryStatistics categoriesNamesLengthStatistics = stream.getCategoriesNamesLengthStatistics(tasks);
        System.out.println("Categories names length statistics: " + categoriesNamesLengthStatistics);

        Task taskWithBiggestCountOfCategories = stream.findTaskWithBiggestCountOfCategories(tasks);
        System.out.println("Task with biggest count of category: " + taskWithBiggestCountOfCategories);
    }
}

package com.geekhub.task3;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class CollectionUtils {

    CollectionUtils() {
    }

    public static <E> List<E> generate(Supplier<E> generator,
                                       Supplier<List<E>> listFactory,
                                       int count) {
        List<E> list = listFactory.get();
        for (int i = 0; i < count; i++) {
            list.add(generator.get());
        }
        return list;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                list.add(element);
            }
        }
        return list;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E elem : elements) {
            if (predicate.test(elem)) {
                return true;
            }
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E elem : elements) {
            if (!predicate.test(elem)) {
                return false;
            }
        }
        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E elem : elements) {
            if (predicate.test(elem)) {
                return false;
            }
        }
        return true;
    }

    public static <T, R> List<R> map(List<T> elements,
                                     Function<T, R> mappingFunction,
                                     Supplier<List<R>> listFactory) {
        List<R> list = listFactory.get();
        for (T elem : elements) {
            list.add(mappingFunction.apply(elem));
        }
        return list;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        List<E> list = new ArrayList<>(elements);
        list.sort(comparator);
        return Optional.of(list.get(list.size() - 1));
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        List<E> list = new ArrayList<>(elements);
        list.sort(comparator);
        return Optional.of(list.get(0));
    }

    public static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> list = listFactory.get();
        Set<E> set = new HashSet<>(elements);
        list.addAll(set);
        return list;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E elem : elements) {
            consumer.accept(elem);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        E e = null;
        for (E elem : elements) {
            e = accumulator.apply(e, elem);
        }
        assert e != null;
        return Optional.of(e);
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E e = seed;
        for (E elem : elements) {
            e = accumulator.apply(e, elem);
        }
        return e;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements,
                                                        Predicate<E> predicate,
                                                        Supplier<Map<Boolean, List<E>>> mapFactory,
                                                        Supplier<List<E>> listFactory) {
        List<E> trueList = listFactory.get();
        List<E> falseList = listFactory.get();

        for (E element : elements) {
            boolean result = predicate.test(element);
            if (result) {
                trueList.add(element);
            } else {
                falseList.add(element);
            }
        }

        Map<Boolean, List<E>> map = mapFactory.get();
        map.put(true, trueList);
        map.put(false, falseList);

        return map;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                                 Function<T, K> classifier,
                                                 Supplier<Map<K, List<T>>> mapFactory,
                                                 Supplier<List<T>> listFactory) {
        Map<K, List<T>> map = mapFactory.get();

        for (T element : elements) {
            K key = classifier.apply(element);
            List<T> group = map.computeIfAbsent(key, k -> listFactory.get());
            group.add(element);
        }
        return map;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction,
                                            Supplier<Map<K, U>> mapFactory) {
        Map<K, U> map = mapFactory.get();

        for (T element : elements) {
            K key = keyFunction.apply(element);
            U value = valueFunction.apply(element);
            U prevValue = map.get(key);
            if (prevValue == null) {
                map.put(key, value);
            } else {
                U mergedElement = mergeFunction.apply(prevValue, value);
                map.put(key, mergedElement);
            }
        }
        return map;
    }

    //ADDITIONAL METHODS

    public static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                        Predicate<E> predicate,
                                                                        Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                        Supplier<List<T>> listFactory,
                                                                        Function<E, T> elementMapper) {
        Map<Boolean, List<T>> map = mapFactory.get();
        List<T> listTrue = listFactory.get();
        List<T> listFalse = listFactory.get();

        map.put(Boolean.TRUE, new ArrayList<>());
        map.put(Boolean.FALSE, new ArrayList<>());

        for (E elem : elements) {
            T elem2 = elementMapper.apply(elem);
            map.get(predicate.test(elem)).add(elem2);
        }

        map.put(Boolean.FALSE, listFalse);
        map.put(Boolean.TRUE, listTrue);

        return map;
    }

    public static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                                 Function<T, K> classifier,
                                                                 Supplier<Map<K, List<U>>> mapFactory,
                                                                 Supplier<List<U>> listFactory,
                                                                 Function<T, U> elementMapper) {
        Map<K, List<U>> map = mapFactory.get();

        for (T elem : elements) {
            K key = classifier.apply(elem);
            if (!map.containsKey(key)) {
                List<U> value = listFactory.get();
                U elem2 = elementMapper.apply(elem);
                value.add(elem2);
                map.put(key, value);
            } else {
                List<U> list = map.get(key);
                U elem2 = elementMapper.apply(elem);
                list.add(elem2);
            }
        }

        return map;
    }
}

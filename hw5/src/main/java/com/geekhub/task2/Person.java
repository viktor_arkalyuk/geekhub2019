package com.geekhub.task2;

public class Person implements Comparable<Person> {
    private String name;
    private int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        int result;
        result = name.compareTo(o.name);
        if (result != 0) {
            return result;
        }
        return Integer.compare(age, o.age);
    }
}

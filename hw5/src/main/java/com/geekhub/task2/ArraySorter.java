package com.geekhub.task2;

class ArraySorter {
    <T extends Comparable<T>> T[] bubbleSort(T[] array, Direction direction) {
        T[] clone = array.clone();
        for (int i = 0; i < clone.length - 1; i++) {
            for (int j = 0; j < clone.length - i - 1; j++) {
                if (clone[j].compareTo(clone[j + 1]) * direction.getDirection() > 0) {
                    T tmp = clone[j];
                    clone[j] = clone[j + 1];
                    clone[j + 1] = tmp;
                }
            }
        }
        return clone;
    }

    <T extends Comparable<T>> T[] insertionSort(T[] array, Direction direction) {
        T[] clone = array.clone();
        for (int i = 1; i < clone.length; i++) {
            for (int j = i; (j > 0) && (clone[j - 1].compareTo(clone[j]) * direction.getDirection() > 0); j--) {
                T tmp = clone[j];
                clone[j] = clone[j - 1];
                clone[j - 1] = tmp;
            }
        }
        return clone;
    }

    <T extends Comparable<T>> T[] selectionSort(T[] array, Direction direction) {
        T[] clone = array.clone();
        for (int i = 0; i < clone.length; i++) {
            int minElement = i;
            for (int j = i + 1; j < clone.length; j++) {
                if (clone[j].compareTo(clone[minElement]) * direction.getDirection() < 0) {
                    minElement = j;
                }
            }
            if (minElement != i) {
                T tmp = clone[i];
                clone[i] = clone[minElement];
                clone[minElement] = tmp;
            }
        }
        return clone;
    }
}

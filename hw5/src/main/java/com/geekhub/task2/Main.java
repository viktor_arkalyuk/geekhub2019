package com.geekhub.task2;

import java.util.Arrays;

public class Main {
    private static Person[] array;
    private static ArraySorter arraySorter = new ArraySorter();

    public static void main(String[] args) {
        createArray();
        testBubbleSort();
        testInsertionSort();
        testSelectionSort();
    }

    private static void testSelectionSort() {
        System.out.println(Arrays.toString(array));
        array = arraySorter.selectionSort(array, Direction.DESC);
        System.out.println(Arrays.toString(array));
        array = arraySorter.selectionSort(array, Direction.ASC);
        System.out.println(Arrays.toString(array));
    }

    private static void testInsertionSort() {
        System.out.println(Arrays.toString(array));
        array = arraySorter.insertionSort(array, Direction.DESC);
        System.out.println(Arrays.toString(array));
        array = arraySorter.insertionSort(array, Direction.ASC);
        System.out.println(Arrays.toString(array));
    }

    private static void testBubbleSort() {
        System.out.println(Arrays.toString(array));
        array = arraySorter.bubbleSort(array, Direction.DESC);
        System.out.println(Arrays.toString(array));
        array = arraySorter.bubbleSort(array, Direction.ASC);
        System.out.println(Arrays.toString(array));
    }

    private static void createArray() {
        array = new Person[5];
        array[0] = new Person("Anna", 12);
        array[1] = new Person("Andrey", 14);
        array[2] = new Person("Victor", 16);
        array[3] = new Person("Sasha", 15);
        array[4] = new Person("Liza", 17);
    }
}

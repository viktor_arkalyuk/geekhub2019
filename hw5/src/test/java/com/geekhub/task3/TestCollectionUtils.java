package com.geekhub.task3;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static com.geekhub.task3.CollectionUtils.*;

public class TestCollectionUtils {
    @Test
    public void testFilter() {
        System.out.println("Testing filter()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        List<Integer> list1 = filter(
                list,
                number -> number > 5);
        System.out.println(list1);
        Assert.assertEquals(3, list1.size());
    }

    @Test
    public void testGenerate() {
        System.out.println("Testing generate()");
        List<Integer> generate = generate(
                () -> 0,
                ArrayList::new, 5);
        System.out.println(generate);
        Assert.assertEquals(5, generate.size());
    }

    @Test
    public void testAnyMatch() {
        System.out.println("Testing anyMatch()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        boolean s = anyMatch(
                list,
                number -> number < 6);
        System.out.println(s);
        Assert.assertTrue(s);
    }

    @Test
    public void testNoneMatch() {
        System.out.println("Testing noneMatch()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        boolean s = noneMatch(
                list,
                number -> number < 3);
        System.out.println(s);
        Assert.assertFalse(s);
    }

    @Test
    public void testAllMatch() {
        System.out.println("Testing allMatch()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        boolean s = allMatch(
                list,
                number -> number < 10);
        System.out.println(s);
        Assert.assertTrue(s);
    }

    @Test
    public void testMap() {
        System.out.println("Testing map()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        List<Integer> map = map(
                list,
                (Integer number) -> number * 2,
                ArrayList::new);
        System.out.println(map);
        Assert.assertEquals(0, map.indexOf(2));
    }

    @Test
    public void testDistinct() {
        System.out.println("Testing distinct()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        List<Integer> distinct = distinct(
                list,
                ArrayList::new);
        System.out.println(distinct);
        Assert.assertEquals(1, distinct.indexOf(2));
    }

    @Test
    public void testMax() {
        System.out.println("Testing max()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        Optional<Integer> max = max(
                list,
                Integer::compareTo);
        System.out.println(max);
        Assert.assertEquals(Optional.of(9), max);
    }

    @Test
    public void testMin() {
        System.out.println("Testing min()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        Optional<Integer> min = min(
                list,
                Integer::compareTo);
        System.out.println(min);
        Assert.assertEquals(Optional.of(1), min);
    }

    @Test
    public void testForeach() {
        System.out.println("Testing foreach()");
        List<Integer> list = Arrays.asList(1, 1, 1, 2, 7, 3, 8, 9);
        forEach(
                list,
                System.out::println);
    }

    @Test
    public void testReduce() {
        System.out.println("Testing reduce()");
        List<String> list = Arrays.asList("Joe", "Johny", "Molly");
        Optional<String> reduce = reduce(
                list,
                (str1, str2) -> str1 + "-" + str2);
        System.out.println(reduce);
    }

    @Test
    public void testReduceSeed() {
        System.out.println("Testing reduceSeed()");
        List<String> list1 = Arrays.asList("Oleg", "Johny", "Molly");
        String reduce1 = reduce(
                "Joe",
                list1,
                (str1, str2) -> str1 + "-" + str2);
        System.out.println(reduce1);
        Assert.assertEquals(0, reduce1.indexOf("Joe"));
    }

    @Test
    public void testPartitionBy() {
        System.out.println("Testing partitionBy()");
        List<String> list = Arrays.asList("Oleg", "Johny", "Molly");
        Map map = new HashMap<Boolean, List<String>>();
        System.out.println(CollectionUtils.partitionBy(
                list,
                (i) -> i.length() > 3,
                () -> map,
                ArrayList::new
        ));
    }

    @Test
    public void testGroupBy() {
        System.out.println("Testing groupBy()");
        Map map1 = new HashMap<Integer, List<String>>();
        List<String> list = Arrays.asList("Oleg", "Johny", "Molly");
        System.out.println(CollectionUtils.groupBy(
                list,
                String::length,
                () -> map1,
                ArrayList::new
        ));
    }

    @Test
    public void toMap() {
        System.out.println("Testing toMap()");
        List<String> listToMap = Arrays.asList("banana", "apple", "apricot", "melon");
        System.out.println(CollectionUtils.toMap(
                listToMap,
                s -> s.charAt(0),
                s -> s,
                (a, b) -> a + ", " + b,
                HashMap::new
        ));
    }

    @Test
    public void testPartitionByAndElement() {
        System.out.println("Testing partitionByAndElement()");
        Map map2 = new HashMap<Boolean, List<String>>();
        List<String> list = Arrays.asList("Oleg", "Johny", "Molly");
        System.out.println(CollectionUtils.partitionByAndMapElement(
                list,
                (i) -> i.length() > 3,
                () -> map2,
                ArrayList::new,
                s -> s + " AFTER MAPPING"
        ));
    }

    @Test
    public void testGroupByAndElement() {
        System.out.println("Testing groupByAndElement()");
        Map map4 = new HashMap<Integer, List<String>>();
        List<String> list = Arrays.asList("Oleg", "Johny", "Molly");
        System.out.println(CollectionUtils.groupByAndMapElement(
                list,
                String::length,
                () -> map4,
                ArrayList::new,
                s -> s + " AFTER MAPPING"
        ));
    }
}
